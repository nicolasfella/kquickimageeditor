# KQuickImageEditor

## Introduction

KQuickImageEditor is a set of QtQuick components providing basic image editing
capabilities.

## License

KQuickImageEditor is a licensed under the Lesser General Public License version
2.1 or later. A copy of the license can be found in
[this repo](LICENSES/LGPL-2.1-or-later.txt).

The examples are licensed under the 2-Clause BSD, that can be found in
[here](LICENSES/BSD-2-Clause.txt).

